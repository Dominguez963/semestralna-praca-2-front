import { Component, OnInit } from '@angular/core';
import { NgbCarouselConfig} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-carousel-basic',
  templateUrl: './carousel-basic.component.html',
  styleUrls: ['./carousel-basic.component.css'],
  providers: [NgbCarouselConfig]
})
export class CarouselBasicComponent implements OnInit {

  constructor() {}

  images = ['home', 'jupiter', 'venus', 'mars', 'mercury', 'saturn'].map((name) => `assets/${name}.png`);

  ngOnInit(): void {
  }

}
