import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {LoginResponse} from '../models/loginResponse';
import {LoginService} from '../login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  private httpOption = {headers: new HttpHeaders({'Content-Type': 'application/json'})};
  message = ' ';

  constructor(public activeModal: NgbActiveModal, private http: HttpClient, private loginService: LoginService) { }

  ngOnInit(): void {
  }

  // tslint:disable-next-line:typedef
  login(name: string, password: string) {
    this.http.post<LoginResponse>('/api/login', { name, password}, this.httpOption).subscribe(response => {
      this.message = response.message;
      if (this.message === 'Úspešne prihlásený') {
        this.loginService.adminIsLogged = true;
        setTimeout(() => this.activeModal.dismiss('Admin logged in'), 1500);
      }
    });
  }
}
