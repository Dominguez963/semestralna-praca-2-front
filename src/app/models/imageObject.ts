export interface ImageObject {
  name: string;
  img: {type: string, data: number[]};
}
