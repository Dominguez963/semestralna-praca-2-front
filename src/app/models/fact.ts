export interface Fact {
  id: number;
  title: string;
  text: string;
  fileName: string;
}
