import { Component, OnInit } from '@angular/core';
import {Post} from '../models/post';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {LoginService} from '../login.service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  private httpOption = {headers: new HttpHeaders({'Content-Type': 'application/json'})};

  editingContent: string;
  editingTitle: string;
  editingId = -1;
  input: Post = {} as Post;
  posts = [] as Post[];
  constructor(private http: HttpClient, public loginService: LoginService) { }

  ngOnInit(): void {
    this.loadData();
  }

  add(): void {
    if (this.input.author != null && this.input.content != null && this.input.title != null) {
      this.http.post<Post>('api/addPost', this.input, this.httpOption).subscribe(response => {
        this.posts.push(response[0]);
        this.input = {} as Post;
      });
    }
  }
  remove(id: number): void {
    if (confirm('Naozaj chcete vymazať tento príspevok ?')) {
      this.http.delete(`/api/deletePost/${id}`, this.httpOption).subscribe(response => {
        this.posts.splice(this.posts.indexOf(this.posts.filter((post) => id === post.id)[0]), 1);
      });
    }

  }
  edit(id: number, content: string, title: string): void {
    this.http.put('/api/editPost', { content: this.editingContent, title: this.editingTitle, id: this.editingId },
      this.httpOption).subscribe(response => {
      this.posts.filter((post) => id === post.id)[0].content = content;
      this.posts.filter((post) => id === post.id)[0].title = title;
      this.editingId = -1;
    });
  }
  startEdit(id: number): void {
    this.editingContent = this.posts.filter((post) => id === post.id)[0].content;
    this.editingTitle = this.posts.filter((post) => id === post.id)[0].title;
    this.editingId = id;
  }
  loadData(): void {
    this.http.get('/api/posts', this.httpOption).subscribe(response => this.posts = response as Post[]);
  }
}
