import { Component } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import {LoginComponent} from './login/login.component';
import {LoginService} from './login.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Semv2';
  isCollapsed = false;

  constructor(private modalService: NgbModal, public loginService: LoginService) {}

  // tslint:disable-next-line:typedef
  login() {
    this.modalService.open(LoginComponent);
  }

  // tslint:disable-next-line:typedef
  logout() {
    if (confirm('Naozaj sa chcete odhlásiť?')) {
      this.loginService.adminIsLogged = false;
    }
  }
}
