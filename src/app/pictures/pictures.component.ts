import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, ParamMap} from '@angular/router';
import {Post} from '../models/post';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {ImageObject} from '../models/imageObject';

@Component({
  selector: 'app-pictures',
  templateUrl: './pictures.component.html',
  styleUrls: ['./pictures.component.css']
})
export class PicturesComponent implements OnInit {

  private httpOption = {headers: new HttpHeaders({'Content-Type': 'application/json'})};

  images: ImageObject[];
  pictureNames: string[];
  name: string;

  constructor(private route: ActivatedRoute, private http: HttpClient) { }

  ngOnInit(): void {
    this.route.params.subscribe(value => {
      this.name = value.name;
      if (this.name === 'milkyWay') {
        this.pictureNames = ['milkway', 'milkway1', 'milkway2', 'milkyway10', 'milkyway11', 'milkyway12'];
      } else if (this.name === 'solarSystem') {
        this.pictureNames = ['sun', 'mercury', 'venus', 'earth', 'mars', 'jupiter', 'saturn',
          'uran', 'neptune', 'pluto', 'solar_system'];
      }
      this.loadData();
    });
  }
  loadData(): void {
    this.http.get('/api/images', this.httpOption).subscribe(response => {
      this.images = response as ImageObject[];
      console.log(this.images);
    });
    /*const TYPED_ARRAY = new Uint8Array(this.images[0].img.data);
    const STRING_CHAR = TYPED_ARRAY.reduce((data, byte) => {
      return data + String.fromCharCode(byte);
    }, '');
    const base64String = btoa(STRING_CHAR);
    this.imageurl = this.domSanitizer.bypassSecurityTrustUrl(base64String);*/
  }
}
