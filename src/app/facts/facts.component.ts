import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Fact} from '../models/fact';
import {DeletedFact} from '../models/deletedFact';
import {LoginService} from '../login.service';

@Component({
  selector: 'app-facts',
  templateUrl: './facts.component.html',
  styleUrls: ['./facts.component.css']
})
export class FactsComponent implements OnInit {

  private httpOption = {headers: new HttpHeaders({'Content-Type': 'application/json'})};

  facts: Fact[];
  deletedFacts: number[];
  constructor(private http: HttpClient, public loginService: LoginService) { }

  ngOnInit(): void {
    this.loadData();
  }

  loadData(): void {
    this.http.get('/api/facts', this.httpOption).subscribe(response => this.facts = response as Fact[]);
    this.http.get<DeletedFact[]>('/api/deletedFacts', this.httpOption).subscribe(
      response => {
        this.deletedFacts = [] as number[];
        for (const item of response) {
          this.deletedFacts.push(item.idVymazaneho);
        }
      });
  }

  // tslint:disable-next-line:typedef
  removeFact(id: number) {
    if (confirm('Naozaj chcete vymazať danú zaujímavosť ?')) {
      this.http.delete(`api/deleteFact/${id}`, this.httpOption).subscribe(() => {
        this.deletedFacts.push(id);
      });
    }
  }

  // tslint:disable-next-line:typedef
  revertChanges() {
    if (confirm('Naozaj chcete vrátiť všetky vykonané zmeny nad Zaujímavosťami ?')) {
      this.http.delete('api/revertChanges', this.httpOption).subscribe( () => {
        this.deletedFacts = [] as number[];
      });
    }
  }
}
