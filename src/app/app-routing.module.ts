import { NgModule } from '@angular/core';
import { RouterModule, Routes, ParamMap } from '@angular/router';
import {PostsComponent} from './posts/posts.component';
import {HomeComponent} from './home/home.component';
import {PicturesComponent} from './pictures/pictures.component';
import {FactsComponent} from './facts/facts.component';
import {ContactComponent} from './contact/contact.component';


const routes: Routes = [
  {
    path: '', redirectTo: '/home', pathMatch: 'full'
  },
  {
    path: 'posts', component: PostsComponent
  },
  {
    path: 'home', component: HomeComponent
  },
  {
    path: 'facts', component: FactsComponent
  },
  {
    path: 'contact', component: ContactComponent
  },
  {
    path: 'pictures/:name', component: PicturesComponent
  }
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
