const PROXY_CONFIG = [
  {
    context: ['/api'],
    target: 'http://localhost:5500',
    secure: false
  }
]
module.exports = PROXY_CONFIG;
